/* NOTE: Values to mimic certain materials's effects from light: http://devernay.free.fr/cours/opengl/materials.html
 *
 *
 *
 *
 *
 *
 *
 *
 */


#include <GL/glew.h> // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h> // Located in glfw WIN64 library -> NOTE: the architecture (e.g. WIN64) is what your app is targeting
#include <GL/gl.h> // Located in MinGW include folder

//#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <math.h>

#include "engine.h"

int main( int argc, char** argv )
{

    struct FYEngine engine;
    CreateEngine( &engine );

    /*glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); // Specify how OGL should blend alpha pixels
    glEnable( GL_BLEND ); // We want to blend, so enable it*/

    glEnable( GL_DEPTH_TEST ); // Ensure that near vertices overlap far vertices and not vice-versa

    /*glEnable( GL_CULL_FACE ); 
    glEnable( GL_BACK );*/

    float cube_vertices[] = {
        // Back face                Due to >this< Normal is pointing in -Z direction (also the z values are fixed and negative)
        // positions          // normals           // texture coords
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

        // Front face
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
    };

    float pointLightPositions[4][3] = {
        { 0.7f,  0.2f,  2.0f },
        { 2.3f, -3.3f, -4.0f },
        { -4.0f,  2.0f, -12.0f },
        { 0.0f,  0.0f, -3.0f }
    };

    
    // REGION: CUBE VAO
    // NOTE: Diffuse maps is just a term that means a textured object in a lighted scene
    //       They stand in for the diffuse value used in lighting calculations (the sampled texture coords at location texCoords)
    //       Diffuse value is (usually...) always the same as ambient (both should represent object's true perceived color)
    //       and thus we can use the single value for both, allowing us to save space, but you could have a separate ambient 
    //       value if want
    unsigned int VBO, cubeVAO;
    glGenVertexArrays( 1, &cubeVAO );
    glGenBuffers( 1, &VBO );
    glBindVertexArray( cubeVAO );
    glBindBuffer( GL_ARRAY_BUFFER, VBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0 );
    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)) );
    glEnableVertexAttribArray( 1 );
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)) );
    glEnableVertexAttribArray( 2 );
    glBindVertexArray( 0 );


    unsigned int diffuse_map_id;
    glGenTextures( 1, &diffuse_map_id );
    glBindTexture( GL_TEXTURE_2D, diffuse_map_id );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

    int width, height, numChannels;
    unsigned char* data = stbi_load( "rsc/textures/container2.png", &width, &height, &numChannels, 3 );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data );
    stbi_image_free( data );

    glBindTexture( GL_TEXTURE_2D, 0 );



    // NOTE: Specular maps provide specular intensity values (the shininess parts of the object)
    //       This is done by taking the object's original texture and blacking out the parts that shouldn't have reflection
    //       (e.g. wood) and then turning result into black and white so that the grayscale allows us to only care about the 
    //       intensities of the bright spot. We could add color to the specular map to create a different color of specular light,
    //       but this (~pretty much...) never occurs realistically in nature and is not realistic to have in a realistic game.
    unsigned int specular_map_id;
    glGenTextures( 1, &specular_map_id );
    glBindTexture( GL_TEXTURE_2D, specular_map_id );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

    data = stbi_load( "rsc/textures/container2_specular.png", &width, &height, &numChannels, 3 );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data );
    stbi_image_free( data );
    
    glBindTexture( GL_TEXTURE_2D, 0 );





    // REGION: LIGHT VAO
    unsigned int lightVAO;
    glGenVertexArrays( 1, &lightVAO );
    glBindVertexArray( lightVAO );
    glBindBuffer( GL_ARRAY_BUFFER, VBO );
    // We ignore the 3 normal data points for the light object, this actually more efficient than creating a separate VBO
    //      Because the data is already in the GPU's memory from the cube object above.
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0 );
    glEnableVertexAttribArray( 0 );
    glBindVertexArray( 0 );

    // TODO: have this in engine as some "CreateFPSMatrices" function?
    struct Vector3f lamp_position = { 3.0f, 8.0f, 0 };
    struct Matrix4f light_model_to_world = CreateTransformationMatrix4f( &lamp_position, 0, 0, 0, 1.0f );
    struct Matrix4f world_to_camera = CreateViewMatrix4f( &engine.camera.position, &engine.camera.target, &engine.camera.up );
    struct Matrix4f camera_to_clip = CreatePerspectiveMatrix4f( 45.0f, SCREEN_WIDTH / (float) SCREEN_HEIGHT, 0.1f, 100.0f );
    LoadScreenProjectionMatrix( engine.shaderPrograms[SHADER_CATEGORY_TEXT].id, (float) SCREEN_WIDTH, (float) SCREEN_HEIGHT );
    LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], &light_model_to_world, &world_to_camera, &camera_to_clip );
    LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], 0, &world_to_camera, &camera_to_clip );



    // Structs in GLSL are just encapsulation of uniform variables, so still need to set the unifrom for each, now with prefix
    // REGION: Material uniforms
    // Set the ambient AND the diffuse to the collor we'd like our object to have
    /*glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "material.ambient"),
                 1.0f, 0.5f, 0.31f );*/
    // Set the ambient AND the diffuse to the collor we'd like our object to have
    // NOTE: It seems we pass in the "ActiveTexture" value we expect to have? e.g. GL_TEXTURE0
    // NOTE: Flipping these causes the diffuse lighting to be from the black/white specular texture one and the specular
    //       lighting to be from the normal object's texture image.
    //       It provides a cool effect of having the "bright spot" be the original texture, where the rest of the object
    //       is grayed out with the black and white specular intensity image (e.g. the board is all that shows (in gray)
    // NOTE: Below we assign a location value to the texture sampler (so we can have multiple textures at once).
    //          This is required for some drivers. So the fact that the sampler2D is a uniform in our shader doesn't mean that
    //          we need to pass in the texture data to the uniform, ALL WE NEED TO DO IS SPECIFY WHICH TEXTURE UNIT EACH
    //          SHADER SAMPLER BELONGS TO (by setting each sampler using glUniform1i)
    //          This makes sure that each uniform sampler corresponds to the proper texture unit (as some drivers need told)
    //          This allows us to assign location to texture sampler and ultimately allow us set multiple textures 
    //          at once in the fragment shader. The main purpose of a texture unit is to allow us to use >1 texture in shader
    //          YOU MUST BE SURE TO BIND THE TEXTURE THAT CORRESPONDS TO THE DESIRED ACTIVE TEXTURE
    //          This means, glActiveTexture(GL_TEXTURE0); glBindTexture(..., 0) where 0 is TEXTURE WHO's UNIFORM WE SET to 0
    // NOTE: This and material.specular can be set once outside of loop
    glUniform1i( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "material.diffuse"), 0 );
    // Usually set to some medium-bright color (depending on what material we want to simulate)
    glUniform1i( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "material.specular"), 1 );
    glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "material.shininess"), 32.0f );

    float x = 0.0;
    while( engine.windowState.running )
    {
        ProcessInput( &engine.windowState, 0, 0, &engine.camera );
        RenderPrepare();

        engine.camera.target = AddVector3f( &engine.camera.position, &engine.camera.front );
        world_to_camera = CreateViewMatrix4f( &engine.camera.position, &engine.camera.target, &engine.camera.up );

        LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], 0, &world_to_camera, 0 );
        LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], 0, &world_to_camera, 0 );

        /*struct Vector3f object_trans = { 0, 0, 1 }; x += 0.1f;
        struct Matrix4f object_model_to_world = CreateTransformationMatrix4f( &object_trans, x, x, 0, 1.0f );
        LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], &object_model_to_world, 0, 0 );*/

        UseProgram( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id );
        // REGION: 
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "viewPos"), 
                     engine.camera.position.components[0], engine.camera.position.components[1], 
                     engine.camera.position.components[2] );




        // REGION: DIRECTIONAL LIGHT (e.g. Sun)
        // Directional light -> w component = 0.0f
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "directionalLight.direction"), 
                     -0.2f, -1.0f, -0.3f );

        // Usually set to low intensity since we don't want ambient light to be dominant
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "directionalLight.ambient"), 
                                           0.2f, 0.2f, 0.2f );
        // Usually set to the exact color we'd like a light to have (usually bright white)
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "directionalLight.diffuse"), 
                                           0.5f, 0.5f, 0.5f );
        // Usually set to vec3(1.0) shining at full intensity
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "directionalLight.specular"), 
                                           1.0f, 1.0f, 1.0f );






        // REGION: POINT LIGHTS
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[0].constant"), 1.0f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[0].linear"), 0.09f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[0].quadratic"), 0.032f );
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[0].ambient"), 
                                           0.2f, 0.2f, 0.2f );
        // Usually set to the exact color we'd like a light to have (usually bright white)
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[0].diffuse"), 
                                           0.5f, 0.5f, 0.5f );
        // Usually set to vec3(1.0) shining at full intensity
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[0].specular"), 
                                           1.0f, 1.0f, 1.0f );
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[0].position"), 
                                           pointLightPositions[0][0], pointLightPositions[0][1], pointLightPositions[0][2] );

        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[1].constant"), 1.0f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[1].linear"), 0.09f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[1].quadratic"), 0.032f );
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[1].ambient"), 
                                           0.2f, 0.2f, 0.2f );
        // Usually set to the exact color we'd like a light to have (usually bright white)
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[1].diffuse"), 
                                           0.5f, 0.5f, 0.5f );
        // Usually set to vec3(1.0) shining at full intensity
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[1].specular"), 
                                           1.0f, 1.0f, 1.0f );
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[1].position"), 
                                           pointLightPositions[1][0], pointLightPositions[1][1], pointLightPositions[1][2] );

        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[2].constant"), 1.0f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[2].linear"), 0.09f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[2].quadratic"), 0.032f );
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[2].ambient"), 
                                           0.2f, 0.2f, 0.2f );
        // Usually set to the exact color we'd like a light to have (usually bright white)
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[2].diffuse"), 
                                           0.5f, 0.5f, 0.5f );
        // Usually set to vec3(1.0) shining at full intensity
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[2].specular"), 
                                           1.0f, 1.0f, 1.0f );
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[2].position"), 
                                           pointLightPositions[2][0], pointLightPositions[2][1], pointLightPositions[2][2] );

        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[3].constant"), 1.0f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[3].linear"), 0.09f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[3].quadratic"), 0.032f );
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[3].ambient"), 
                                           0.2f, 0.2f, 0.2f );
        // Usually set to the exact color we'd like a light to have (usually bright white)
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[3].diffuse"), 
                                           0.5f, 0.5f, 0.5f );
        // Usually set to vec3(1.0) shining at full intensity
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[3].specular"), 
                                           1.0f, 1.0f, 1.0f );
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "pointLights[3].position"), 
                                           pointLightPositions[3][0], pointLightPositions[3][1], pointLightPositions[3][2] );



        
        // REGION: Spotlight
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.position"), 
                     engine.camera.position.components[0], engine.camera.position.components[1], 
                     engine.camera.position.components[2] );

        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.direction"), 
                     engine.camera.front.components[0], engine.camera.front.components[1], 
                     engine.camera.front.components[2] );

        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.innerCutoff"), 
                     cos(RADIANS(12.5f)) );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.outerCutoff"), 
                     cos(RADIANS(17.5f)) );

        // Usually set to low intensity since we don't want ambient light to be dominant
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.ambient"), 
                                           0.0f, 0.0f, 0.0f );
        // Usually set to the exact color we'd like a light to have (usually bright white)
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.diffuse"), 
                                           1.0f, 1.0f, 1.0f );
        // Usually set to vec3(1.0) shining at full intensity
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.specular"), 
                                           1.0f, 1.0f, 1.0f );

        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.constant"), 1.0f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.linear"), 0.09f );
        glUniform1f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "spotLight.quadratic"), 0.032f );


        glActiveTexture( GL_TEXTURE0 );
        glBindTexture( GL_TEXTURE_2D, diffuse_map_id );

        glActiveTexture( GL_TEXTURE0 + 1 );
        glBindTexture( GL_TEXTURE_2D, specular_map_id );

        glBindVertexArray( cubeVAO );

        //x+= 0.1f;
        for( int i = 0; i < 10; i++ )
        {
            struct Vector3f object_trans = { (float) i*2 + 5, 0, 0.0f }; //x += 0.1f;
            struct Matrix4f object_model_to_world = CreateTransformationMatrix4f( &object_trans, x, x, 0, 1.0f );
            LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], &object_model_to_world, 0, 0 ); // NOTE: STOPS PROGRAM!!
            
        UseProgram( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id );
            glDrawArrays( GL_TRIANGLES, 0, 36 );
        }

        StopProgram();



        LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], &light_model_to_world, 0, 0 );
        UseProgram( engine.shaderPrograms[SHADER_CATEGORY_LAMP].id );

        glBindVertexArray( lightVAO );
        glDrawArrays( GL_TRIANGLES, 0, 36 );

        for( int i = 0; i < 4; i++ )
        {
            struct Vector3f object_trans = { pointLightPositions[i][0], pointLightPositions[i][1], pointLightPositions[i][2] };
            struct Matrix4f object_model_to_world = CreateTransformationMatrix4f( &object_trans, 0, 0, 0, 1.0f );
            LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], &object_model_to_world, 0, 0 ); // NOTE: STOPS PROGRAM!!
            
        UseProgram( engine.shaderPrograms[SHADER_CATEGORY_LAMP].id );
            glDrawArrays( GL_TRIANGLES, 0, 36 );
        }

        StopProgram();



        UpdateWindowState( &engine.windowState );
    }


    DestroyEngine( &engine );

}




