
/* 
 * We are now ready to move to coordinate systems where our transfomration matrices are most useful (and necessary)
 * There are 5 major coordinate systems important to us
 *      - Local space (object space): The space where the object is the center. 
 *              Coordinates of your object relative to its local origin. These are the coordinate your object begins in.
 *      - World space: The coord system where all your objects live. These coordinates are with respect to the larger world.
 *              Many other objects live in this space.
 *      - View space (eye/camera space): The camera point of view coordinate system -> What the viewer sees
 *      - Clip space: Coordinate system with determined vertices (i.e. which vertices will show up on screen is now known)
 *              The coordinates at this stage are now processed to -1.0 and 1.0 range
 *      - Screen space: This is the OpenGL 2D screen space. This transforms the -1.0 and 1.0 range to a range defined by glViewport
 * 
 * There are 3 matrices involved with the transformations between these coordinate systems:
 *      - Model Matrix: This takes the object/model from LOCAL SPACE to WORLD SPACE
 *      - View Matrix: Takes the WORLD SPACE coordinate system to the VIEW SPACE coordinate system (camera perspective)
 *      - Projection Matrix; Takes VIEW SPACE coordinate system to CLIP SPACE coordinate system (visible vertices determined)
 * 
 * A Viewport Transform is applied to take the CLIP SPACE to SCREEN SPACE -> This is essentially clip space with origin bottom left
 *
 * Further details: -> Nice visualization at (https://learnopengl.com/Getting-started/Coordinate-Systems)
 *      - Local Space: The space your object begins in. If you were to create a 3D model (e.g. in Blender), the origin of your
 *              object would probably be at (0,0,0), even though your object may end up at a different location in the final game
 *              This just means your objects' vertices start in LOCAL SPACE, all vertices are local to the object.
 *              The object is in local space until we apply transformations to it to move it to the world and then screen
 *      - World Space: The world space contains all of your objects. We want to place the objects relative to each other.
 *              The world space allows us to place objects relative to a central "world origin" and thus space our objects out
 *              The coordinates of the object (from local space) are TRANSFORMED (via MODEL matrix) to the world space
 *              This results in our object PLACED in the world space.
 *              The MODEL MATRIX is a transformation that translates, scales and/or rotates your object to place it in the world
 *                  at a location/orientation they belong to.
 *              Analogy: Taking your house and scaling it down (bit too large in local space), translating it to a new town and
 *                  rotating it so that it fits in a spot in line with all of the other houses
 *              The transformation tutorial's transformation matrix can be thought of as a model matrix
 *                  We transformed the local coords of the container to some different place in the world
 *      - View Space: This is the camera space, the space that represents the user's view. This space is from the camera's POV
 *              The world space coordinates are transformed with a combination of translations and rotations to translate/rotate
 *                  the scene so that certain items are transformed to the front of the camera.
 *              This essentially tugs the space shown on screen to how the camera would view it, leaving the user seeing the world
 *                  through the camera's POV
 *              These combined transformations are generally stored in a VIEW MATRIX that transforms world space to view space
 *      - Clip space: Coordinates that are outside of the expected OpenGL range are "clipped", leaving on fragments visible
 *          on your screen to be considered for rendering.
 *          It is often the case that developers will work with objects in their own coordinate system and then convert them to NDC
 *              NDC = Normalized Device Coordinates
 *          This is accomplished by using a PROJECTION MATRIX which maps your own coordinate system range to NDC range
 *              E.g. if our system ranges from -1000 to 1000 in each dimension, the project matrix will transform each coordinate
 *                  WITHIN this specified range to NDC (-1.0 to 1.0). All coordinates outside will be mapped
 *                  So a coordinate of (1250, 500, 750) would not be visisble since the X coordinate is outside of our own range
 *                  The outside of range coordinate (1250) would get mapped to something larger than 1.0 in NDC and thus clipped
 *          If only a part of a primitive (e.g. a triangle) is outside of the "clipping volume" OpenGL will reconstruct the 
 *              triangle as one or more triangles to fit inside of the clipping range. Everything is about primitivees (:
 *          The viewing box a project matrix creates is called a "FRUSTUM" (a 3D thing visible on the 2D screen)
 *              Each coordinate in the FRUSTRUM will end up on the user's screen
 *          The entire process to convert coordinates within a specified range to NDC that can be easily mapped to 2D view space
 *              coordinates is called "PROJECTION", since the PROJECTION MATRIX projects 3D coords to easy-to-map-to-2D NDC.
 *          A final operation called "perspective division" is performed, this divides out x,y,z components of position vectors
 *              by the vector's homogeneous w components. This is what transforms 4D clip space coordinates into 3D NDC
 *              This step is done automatically at the end of each vertex shader run
 *          It is after this stage where resulting coordinates are mapped to screen coordinates (using settings of glViewport)
 *              and later turned into fragments
 *          The projection matrix to transform view coordinates to clip coordinates can take two forms
 *              Each form defines its own unique FRUSTUM (the resulting thing being seen on our 2D screen)
 *                  It is useful to think of our 3D space as being "boxed in" by a "near plane" and a "far plane"
 *                  Things inside of the near plane and far plane region after transforming to clip space will not be clipped
 *              Orthographic Projection: A cube like frustrum is defined, where essentially the near plane and far plane have the
 *                  same size. The ORTHOGRAPHIC PROJECTION MATRIX is what clips things outside of the viewable space.
 *                  The same size near/far planes result in a box like frustrum, things inside box are not clipped.
 *                  This results in no perspective being applied, thus objects far away (closer to far plane) will look the same
 *                  size as the same object closer to the near plane.
 *                  The frustrum defines the visible coordinates and is specified by a WIDTH, HEIGHT, and a NEAR and FAR PLANE
 *                      Width and height define the size of both the far and near planes (since they are the same size)
 *                  The ORTHOGRAPHIC FRUSTRUM maps all coordinates inside the frustrum to NDC 
 *                  An orthographic projection matrican can be made with glm::ortho(...)
 *                  The orthographic frustrum directly maps all coordinates inside the frustrum to normalized device coordinates
 *                      since the w component of each vector is untouched; if the w component is equal to 1, 
 *                      perspective division doesn't change the coordinates
 *              Perspective Projection: Includes perspective, so that the far plane is larger than the near plane
 *                  PERSPECTIVE PROJECTION MATRIX is used to mimic the effect of perspective in the real world
 *                  This matrix maps a given frustrum range to clip space, but ALSO manipulates the w component in such a way that
 *                      the further away a vertex coordinate is from the viewer, the higher this w component becomes.
 *                      Once coordinates are transformed to clip space, they are in the range of -w to w (anything outside=clipped)
 *                      Since OpenGL requires visible coordinate to be between -1.0 and 1.0 as the final vertex shader output,
 *                      once the coordinates are in clip space, PERSPECTIVE DIVISION is applied to the clip space coordinates
 *                      This essentially only involves dividing each component by w -> out = (x/w, y/w, z/w)
 *                      Further away an object is, the higher w is, and thus smaller the resulting size of the object is
 *                      Smaller vertex coordinates are thus given to coordinates further away a vertex is from the viewer
 *                  A projection matrix can be created with the glm::perspective(...) function
 *              When using Orthographic Projection, each vertex coordinate is mapped directly to clip space 
 *                  without any PERSPECTIVE DIVISION (it still happens, but the w component is not manipulated, stays 1)
 *                  Orthographic Projection tends to only be used for 2D renderings and for some architectural/engineering apps
 *                      E.g. Blender uses it for 3D modeling sometimes when we don't want perspective to distort dimensions
 *        The resulting clip coordinates are formed from transmforing a vertex coordinate as follows:
 *              V(clip) = M(projection) * M(view) * M(model) * V(local)       -> Remember: Matrix mult read right to left
 *              I.e. Local coordinate vector transformed by model matrix, then view matrix, and finally the projectio nmatrix
 *              The resulting vertex should then be assigned to gl_Position in the vertex shader and OpenGL will automatically
 *                  perform perspective division and clipping.
 *              NOTE: The output of the vertex shader requires coordinates to be in clip-sapce 
 *                    Which the transformation matrices do
 *                    OpenGL then performs perspective division on the clip-space coordinates to transform them to NDC
 *                    OpenGL then uses the parameters from glViewPort to map the normalized-device coordinates to 
 *                      screen coordinates where each coordinate corresponds to a pont on your screen (e.g. 800x600 screen)
 *                      This process is called the "viewport transform"
 *
 *  See below for implementation details and examples
 */



#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
// Handles window creation and events (minimal library, no rendering functions) -> And cross platform!
#include "GL/glew.h" // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h> // Located in glfw WIN64 library -> NOTE: the architecture (e.g. WIN64) is what your app is targeting
#include <GL/gl.h> // Located in MinGW include folder

#define SCREEN_HEIGHT 600
#define SCREEN_WIDTH 800


void FramebufferSizeCallback( GLFWwindow* window, int width, int height );
unsigned int CompileShader( const char* shaderSource, uint8_t isVertexShader );
unsigned int CreateShaders( const char* vertexShaderSource, const char* fragmentShaderSource );
struct ShaderProgramSource ReadShaderFile( const char* fileLocation );
void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource );
void ProcessInput( GLFWwindow* window );

enum ShaderType
{
    NONE = -1, VERTEX = 0, FRAGMENT = 1
};

struct ShaderProgramSource
{
    char* vertexShaderSource;
    char* fragmentShaderSource;
};

// Screen dimension settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

int main( int argc, char** argv )
{
    if( !glfwInit() )
    {
        printf("Failed to initialize GLFW!\n");
        return -1;
    }

    // Configure the window for API version 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // Specify client API version that created context must be compatible with
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // This is the version after the .
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create the window and the OpenGL context
    GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL /*monitor*/, NULL /*share*/);
    if( !window )
    {
        printf("Failed to create a window!\n");
        glfwTerminate();
        return -1;
    }

    // Make the OpenGL context current, this completes the creation of the OpenGL context
    glfwMakeContextCurrent(window); // Creates the valid OpenGL context

    // Load all of the OpenGL functions
    if( glewInit() != GLEW_OK )
    {
        printf("Failed to initialize glew!\n");
    }

    // Set the frambuffer size callback which will handle setting the viewport size
    //      This is the part of the screen that OpenGL can draw to
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    printf("GL Version: %s\n", glGetString(GL_VERSION));

    struct ShaderProgramSource shaderProgramSource = ReadShaderFile( "rsc/shaders/shaders_tut.shader" );
    unsigned int shaderProgram = CreateShaders( shaderProgramSource.vertexShaderSource, shaderProgramSource.fragmentShaderSource );
    //PrintShaderSource( &shaderProgramSource );

    // Set up vertex data (and buffer(s)) and configure vertex attributes
    /*float vertices[] = {
        // positions          // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 1.0f,   // top right
         0.5f, -0.5f, 0.0f,   1.0f, 0.0f,   // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f,   // bottom left
        -0.5f,  0.5f, 0.0f,   0.0f, 1.0f    // top left 
    };
    int stride_length = 5 * sizeof(float);*/

    // To prevent pixels from being overwritten when being drawn (since OpenGL draws our cube triangle-by-triangle, 
    //      it will overwrite its pixels even though something else might've been drawn there before) we can use the fact
    //      that OpenGL stores depth information in a buffer called the "z-buffer", this allows OpenGL to decide when to draw
    //      over a pixel and when not to. We can use the z-buffer to configure OpenGL to do DEPTH-TESTING
    // OpenGL stores all depth information in a z-buffer (AKA a "depth buffer"). GLFW automatically creates this buffer for us
    //      (just like it has a color-buffer that stores colors of the output image).
    //      The depth is stored within each fragment (as the fragment's z value) and when the fragment wants to output its color,
    //      OpenGL compares its depth value w/ the z-buffer and if the current fragment is behind the other fragment
    //      it is discarded, otherwise overwritten This process is called DEPTH-TESTING, and done automatically by OpenGL
    // We first need to tell OpenGL to automatically perform the depth testing -> enable it (disabled by default)
    glEnable( GL_DEPTH_TEST ); // Enable and Disable functions allows to enable/disable certain functionality of OpenGL
    // In the render loop we will clear the depth buffer (just like clearing the color buffer) by specifying it in glClear
    

    // To render a cube we need 36 total vertices (6 faces * 2 triangles per face * 3 vertices each triangle)
    float vertices[] = {
        // Vertices           // Texture coordinates
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    };
    int stride_length = 5 * sizeof(float);

    // This defines our translation vector for each cube that specifies its position in world space
    // We want to have 10 cube positions, so essentially take our single object and have 10 of them.
    // Each cube is defined as the above, we just will place 10 in our world, this requires modification to model matrix
    // In the game loop we will just call glDrawArrays 10 times, sending a different model matrix to the vertex shader each time
    //      through this loop. This can be seen in the game loop below
    glm::vec3 cubePositions[] = {
          glm::vec3( 0.0f,  0.0f,  0.0f), 
          glm::vec3( 2.0f,  5.0f, -15.0f), 
          glm::vec3(-1.5f, -2.2f, -2.5f),  
          glm::vec3(-3.8f, -2.0f, -12.3f),  
          glm::vec3( 2.4f, -0.4f, -3.5f),  
          glm::vec3(-1.7f,  3.0f, -7.5f),  
          glm::vec3( 1.3f, -2.0f, -2.5f),  
          glm::vec3( 1.5f,  2.0f, -2.5f), 
          glm::vec3( 1.5f,  0.2f, -1.5f), 
          glm::vec3(-1.3f,  1.0f, -1.5f)  
    };

    unsigned int indices[] = {
        0, 1, 3, // first triangle
        1, 2, 3 // second triangle
    };

    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    // Bind Vertex Array Object first, then bind and set vertex buffer(s), and configure vertex attribute(s)
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride_length, (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride_length, (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // To use textures... First load them into your application, we will use stb_image.h from open source stb project
    int width, height, nrChannels;
    unsigned char* data = stbi_load("rsc/container.jpg", &width, &height, &nrChannels, 0);

    if( !data )
    {
        printf("Failed to load texture!\n");
    }

    // REGION Texture handling
    // Textures (like any of our previous OpenGL objects) are referenced by an ID
    unsigned int texture1, texture2;
    glGenTextures(1, &texture1); // Generate 1 texture and store them in our unsigned int array given as second arg (here just 1)
    // Just like other objects, we need to bind it so any subsequent commands will configure the currently bound texture
    glBindTexture(GL_TEXTURE_2D, texture1);

    // Set texture wrapping parameters -> To GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Set the texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Texutres are bound, so now start generating the texture using our loaded image data
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    
    // glGenerateMipmap will automatically generate all mipmaps for the currently bound texture for us.
    glGenerateMipmap(GL_TEXTURE_2D);

    // Loading another texture ...
    // Free the image memory of the previous
    stbi_image_free(data);

    glGenTextures(1, &texture2);
    glBindTexture(GL_TEXTURE_2D, texture2);

    // Set texture wrapping parameters -> To GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Set the texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // We use stbi to flip the bit ordering for us since our image needs an origin at bottom left (:
    stbi_set_flip_vertically_on_load(1); // 1 = TRUE
    data = stbi_load("rsc/awesomeface.png", &width, &height, &nrChannels, 0);
    
    if( !data )
    {
        printf("Failed to load texture!\n");
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    // First activate the shader program so we can get the uniform location of the textures...
    glUseProgram(shaderProgram);
    // Tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
    glUniform1i( glGetUniformLocation(shaderProgram, "texture1"), 0);
    glUniform1i( glGetUniformLocation(shaderProgram, "texture2"), 1);

    stbi_image_free(data);

    glUseProgram(shaderProgram);

    while( !glfwWindowShouldClose(window) )
    {
        ProcessInput( window );

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        // Activate texture unit first before rendering texture. OpenGL has a minimum of 16 texture units for use
        // These texture units are defined in order, so you could get to GL_TEXTURE8 via "GL_TEXTURE0 + 8" (e.g. in a loop)
        glActiveTexture(GL_TEXTURE0); // texture unit TEXTURE0 is always by default activated, so if only 1 texture, no need
        // Bind our texture to the GL_TEXTURE_2D object
        glBindTexture(GL_TEXTURE_2D, texture1); // Bind texture to the CURRENTLY ACTIVE texture unit
        // Do the same for the second texture
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);

        glUseProgram(shaderProgram);

        glm::mat4 transform;
        // The translation part essentially puts it somewhere other than the center (here it is 0.5 along x axis, -.5 on y axis)
        //transform = glm::translate(transform, glm::vec3(0.5f, -0.5f, 0.0f));
        //transform = glm::rotate(transform, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));

        // This creates an orthographic projection matrix
        // Arguments 1 through 4 specify the size of the near and far planes
        // Arg 1 and 2: Specify the left and right coordinate of frustrum
        // Arg 3 and 4: Specify the bottom and top part of frustrum
        // Arguments 5 and 6 define the distances between near and far plane
        // Arg 5 and 6: Specify disntances between near and far plane
        // This projection matrix will transform all coordinates between these x, y, and z ranges to normalized device coordinates
        //glm::ortho(0.0f, 800.f, 0.0f, 600.0f, 0.1f, 100.0f);

        // This creates a perspective projection matrix
        // This also creates a large frustrum (like ortho), anything outside will not be visible (i.e. clipped)
        // Each coordinate inside the volume that is between near plane and far plane (as rays come out of camera) is mapped to
        // a point in clip space. 
        // Arg 1: Defines the FOV (field of view) value -> Sets how large the viewspace is. Usually 45 deg for realistic view
        //        You can achieve a "doom-style" results by setting this to a higher value
        // Arg 2: Sets the aspect ratio, calculated by dividing the viewport's width by its height
        // Arg 3 and 4: Set distance of near and far plane of frustum. Near distance usually 0.1f and far usually 100.0f
        //        All of the vertices between the near and far plane and within the frustrum (using FOV) will be rendered
        // NOTE: If the "near" value of perspective matrix is set too high (like 10.0f), OpenGL will clip all coordinates
        //       close to the camera (between 0.0f and 10.0f) which causes the familiar result seen in games when you move
        //       too close to them and can see inside certain objects.
        //glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)width/(float)height, 0.1f, 1.00f);

        // This model matrix is now handled in the for loop below to take care of moving all 10 cubes to world space
        //glm::mat4 model; // The model matrix to transform our object from local space to world space
        //model = glm::rotate( model, glm::radians(-55.0f), glm:: vec3(1.0f, 0.0f, 0.0f));
        // Rotating the model over time
        //model = glm::rotate( model, (float)glfwGetTime() * glm::radians(50.0f), glm::vec3(0.5f, 1.0f, 0.0f) );
        // By multiplying the vertex coordinates with this model, we're transforming vertex coords to world coords
        // Now create the view matrix -> This will move us slightly backwards in the scene so the object becomes visible
        //    when we are located at origin (0,0,0) in world space
        //    To move around in a scene, consider that moving a camera backwards is the same as moving the entire scene forward
        //    This is exactly what the view matrix does -> We move the entire scene around inversed to where we want camera to move
        //      This gives the impression of moving backwards
        // OpenGL is a "Right-handed system" (by convention)
        //    This means that the positive x-axis is to the right, the positive y-axis is up, and positive z axis is backwards
        //      or towards you. This can be seen by holding right arm up, thumb pointing to right, index up, and middle to you (z)
        //    The "left-handed system" is essentially this idea but with the left hand, so left is pos x axis
        //    The projection matrix switches the handedness
        glm::mat4 view; // The view matrix
        // NOTE: We are translating the scene in the reverse direction of where we want to move
        //          since moving scene away from us is same as moving us backward (which is the goal of this view at the moment)
        view = glm::translate( view, glm::vec3(0.0f, 0.0f, -3.0f) );

        // Now the projection matrix, we want to use perspective projection for our scene
        glm::mat4 projection;
        projection = glm::perspective( glm::radians(45.0f), (float) SCR_WIDTH / (float) SCR_HEIGHT, 0.1f, 100.0f );
        // We want to then pass the transformation matrices to our shaders (done by creating unifroms in our shader)
        // Now to send the matrices to the shader (done each iteration since transformation matrices tend to change a lot)
        //glUniformMatrix4fv( glGetUniformLocation( shaderProgram, "model" ), 1, GL_FALSE, glm::value_ptr(model) );
        glUniformMatrix4fv( glGetUniformLocation( shaderProgram, "view" ), 1, GL_FALSE, glm::value_ptr(view) );
        glUniformMatrix4fv( glGetUniformLocation( shaderProgram, "projection" ), 1, GL_FALSE, glm::value_ptr(projection) );
        //glDrawArrays(GL_TRIANGLES, 0, 36); -> We are now doing this for 10 cubes in a for loop below

        unsigned int transformLoc =  glGetUniformLocation( shaderProgram, "transform" ); // Get location of uniform variable
        glUniformMatrix4fv( transformLoc, 1, GL_FALSE, glm::value_ptr(transform) ); // Send data to shaders

        glBindVertexArray( VAO );

        for( unsigned int i = 0; i < 10; i++ )
        {
            glm::mat4 model;
            model = glm::translate( model, cubePositions[i] );
            model = glm::rotate( model, glm:: radians(20.0f * i), glm::vec3(1.0f, 0.3f, 0.5f) );
            glUniformMatrix4fv( glGetUniformLocation( shaderProgram, "model" ), 1, GL_FALSE, glm::value_ptr(model) );

            glDrawArrays( GL_TRIANGLES, 0, 36 );
        }
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_TRIANGLES, 0, 3);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // Deallocate all resources
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    glfwTerminate(); // Clean/delete all of GLFW's resources that were allocated
    return 0;
}


// This is needed because we need to update the viewport for OpenGL (the rendering area) every time the screen is resized
void FramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0,0, width, height); // This specifies where all the OpenGL rendering is to be displayed

}

// REGION: HANDLE INPUT
// A function to handle input so that it is organized
void ProcessInput(GLFWwindow* window)
{
    // Handle the event that the escape key is pressed -> Close the window
    if( glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS )
    {
        glfwSetWindowShouldClose(window, 1); // Second argument is 1 indicating true
    }
}


// REGION: HANDLE SHADERS
// Can compile either a vertex shader or a fragment shader
unsigned int CompileShader( const char* shaderSource, uint8_t isVertexShader )
{
    // Create the shader
    unsigned int shader = glCreateShader( isVertexShader ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER );
    glShaderSource( shader, 1, &shaderSource, NULL );

    // Compile the source
    glCompileShader(shader);

    int success;
    glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
    if( !success )
    {
        char infoLog[512];
        glGetShaderInfoLog( shader, 512, NULL, infoLog );
        printf("ERROR::SHADER::%s::COMPILATION_FAILED -> %s\n", isVertexShader ? "VERTEX" : "FRAGMENT", infoLog);
    }

    return shader;
}

// Creates both the vertex shader and the fragment shader
unsigned int CreateShaders( const char* vertexShaderSource, const char* fragmentShaderSource )
{
    unsigned int shader_program;

    unsigned int vertex_shader = CompileShader( vertexShaderSource, 1 );
    unsigned int fragment_shader = CompileShader( fragmentShaderSource, 0 );
    

    // Create the program, attach shaders, and then link the shaders to the single program
    shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);

    int success;
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if( !success )
    {
        char infoLog[512];
        glGetProgramInfoLog(shader_program, 512, NULL, infoLog);
        printf("ERROR::PROGRAM::SHADERS::LINKING_FAILED -> %s\n", infoLog);
    }
    
    // Delete the shaders since they are now linked to the shader program (we don't need these references anymore)
    glDeleteShader( vertex_shader );
    glDeleteShader( fragment_shader );

    return shader_program;
}

int StringContains( const char* str, int strLen, const char* fragStr, int fragLen )
{
    for( int i = 0; i < strLen; i++ )
    {
        if( str[i] == fragStr[0] )
        {
            // str[i] == fragStr[0] so move to the next character for comparison
            int strLoc = i+1;
            // Loop through str and fragStr and compare each character
            int fragLoc = 1;
            for( ; fragLoc < fragLen && strLoc < strLen; fragLoc++, strLoc++ )
            {
                // If they differ at any spot, we do not have a match, keep going
                if( str[strLoc] != fragStr[fragLoc] )
                {
                    break;
                }
            }

            // If we ended with the fragment string location being the same as the fragment string length, then we have a match
            if( fragLoc == fragLen )
            {
                return 1;
            }
        }
    }
    return 0;
}

struct ShaderProgramSource ReadShaderFile( const char* fileLocation )
{
    int buffer = 255;
    enum ShaderType type = NONE;

    FILE* fp = fopen( fileLocation, "r" );

    // Skip any leading white space
    while( isspace( fgetc(fp) ) ) ;

    char line[buffer];

    // The shader source buffer that will ultimately be returned in a structure
    char* shaderSource[2] = { (char*) malloc(sizeof(char)), (char*) malloc(sizeof(char)) };
    shaderSource[0][0] = '\0';
    shaderSource[0][1] = '\0';

    while( !feof(fp) )
    {
        // Read in a line of text
        fgets( line, buffer, fp );

        if( StringContains( line, buffer, "#|", 2 ) )
        {
            const char* vertexShaderStr = "|Vertex Shader|";
            const char* fragmentShaderStr = "|Fragment Shader|";

            if( StringContains( line, buffer, vertexShaderStr, strlen(vertexShaderStr) ) )
            {
                type = VERTEX;
            }
            else if( StringContains( line, buffer, fragmentShaderStr, strlen(fragmentShaderStr) ) )
            {
                type = FRAGMENT;
            }
        }
        else
        {
            if( type >= 0 )
            {
                // Add on the line to the shader source. Include length of string to have null terminating character
                shaderSource[type] = (char*) realloc( shaderSource[type], 
                                                        sizeof(char) * (strlen(shaderSource[type]) + strlen(line) + 0) );
                strcat( shaderSource[type], line );
            }
            else
            {
                printf("The shader type was unable to be determined! Source file read: %s\n", fileLocation);
            }
        }
    }

    // Capture the return value before freeing the shaderSource buffer memeory
    struct ShaderProgramSource shaderProgramSource = { shaderSource[VERTEX], shaderSource[FRAGMENT] };

    free( shaderSource[VERTEX] );
    free( shaderSource[FRAGMENT] );

    return shaderProgramSource;
}

void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource )
{
    printf("\n------------VERTEX SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->vertexShaderSource);

    printf("\n------------FRAGMENT SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->fragmentShaderSource);
}





