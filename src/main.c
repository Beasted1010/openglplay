/* NOTE: 
 *
 *
 *
 *
 *
 *
 *
 *
 */


#include <GL/glew.h> // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h> // Located in glfw WIN64 library -> NOTE: the architecture (e.g. WIN64) is what your app is targeting
#include <GL/gl.h> // Located in MinGW include folder

//#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <math.h>

#include "engine.h"

int main( int argc, char** argv )
{

    struct FYEngine engine;
    CreateEngine( &engine );

    /*glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); // Specify how OGL should blend alpha pixels
    glEnable( GL_BLEND ); // We want to blend, so enable it*/

    glEnable( GL_DEPTH_TEST ); // Ensure that near vertices overlap far vertices and not vice-versa

    /*glEnable( GL_CULL_FACE ); 
    glEnable( GL_BACK );*/

    float cube_vertices[] = {
        // Back face                Due to >this< Normal is pointing in -Z direction (also the z values are fixed and negative)
        // positions          // normals           // texture coords
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

        // Front face
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
    };

    /*float pointLightPositions[4][3] = {
        { 0.7f,  0.2f,  2.0f },
        { 2.3f, -3.3f, -4.0f },
        { -4.0f,  2.0f, -12.0f },
        { 0.0f,  0.0f, -3.0f }
    };*/

    struct Vector3f pointLightPositions[7] = {
        { 0.7f,  0.2f,  2.0f },
        { 3.0f,  0.0f,  0.0f },
        { 2.3f, -3.3f, -4.0f },
        { -4.0f,  2.0f, -12.0f },
        { 0.0f,  0.0f, -3.0f },
        { -8.0f,  0.0f, 0.0f },
        { -3.0f,  0.0f, 0.0f }
    };

    
    // REGION: CUBE VAO
    // NOTE: Diffuse maps is just a term that means a textured object in a lighted scene
    //       They stand in for the diffuse value used in lighting calculations (the sampled texture coords at location texCoords)
    //       Diffuse value is (usually...) always the same as ambient (both should represent object's true perceived color)
    //       and thus we can use the single value for both, allowing us to save space, but you could have a separate ambient 
    //       value if want
    unsigned int VBO, cubeVAO;
    glGenVertexArrays( 1, &cubeVAO );
    glGenBuffers( 1, &VBO );
    glBindVertexArray( cubeVAO );
    glBindBuffer( GL_ARRAY_BUFFER, VBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0 );
    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)) );
    glEnableVertexAttribArray( 1 );
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)) );
    glEnableVertexAttribArray( 2 );
    glBindVertexArray( 0 );


    // REGION: LIGHT VAO
    unsigned int lightVAO;
    glGenVertexArrays( 1, &lightVAO );
    glBindVertexArray( lightVAO );
    glBindBuffer( GL_ARRAY_BUFFER, VBO );
    // We ignore the 3 normal data points for the light object, this actually more efficient than creating a separate VBO
    //      Because the data is already in the GPU's memory from the cube object above.
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0 );
    glEnableVertexAttribArray( 0 );
    glBindVertexArray( 0 );

    // TODO: have this in engine as some "CreateFPSMatrices" function?
    struct Matrix4f world_to_camera = CreateViewMatrix4f( &engine.camera.position, &engine.camera.target, &engine.camera.up );
    struct Matrix4f camera_to_clip = CreatePerspectiveMatrix4f( 45.0f, SCREEN_WIDTH / (float) SCREEN_HEIGHT, 0.1f, 100.0f );
    LoadScreenProjectionMatrix( engine.shaderPrograms[SHADER_CATEGORY_TEXT].id, (float) SCREEN_WIDTH, (float) SCREEN_HEIGHT );
    LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], 0, &world_to_camera, &camera_to_clip );
    LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], 0, &world_to_camera, &camera_to_clip );

    struct Material boxMaterial;
    InitializeMaterial( &engine.loader, "rsc/textures/container2.png", "rsc/textures/container2_specular.png", 32.0f, &boxMaterial );
    UseProgram( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id );
    LoadMaterial( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, &boxMaterial );
    StopProgram();

    CreateAndAddDirectionalLightToLighting( { -0.2, -1.0f, -0.3 }, { {0.2f, 0.2f, 0.2f}, {0.5f, 0.5f, 0.5f}, 
                                            {1.0f, 1.0f, 1.0f} }, "dirLight0", &engine.lighting );
    CreateAndAddDirectionalLightToLighting( { -0.2, -1.0f, -0.3 }, { {0.2f, 0.2f, 0.2f}, {0.9f, 0.9f, 0.9f}, 
                                            {1.0f, 1.0f, 1.0f} }, "dirLight1", &engine.lighting );

    CreateAndAddSpotLightToLighting( engine.camera.position, engine.camera.front, 12.5f, 17.5f, 
                                     { {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f, 1.0f} }, { 1.0f, 0.09f, 0.032f }, 
                                    "spotLight0", &engine.lighting );

    float x = 0.0;
    while( engine.windowState.running )
    {
        ProcessInput( &engine.windowState, 0, 0, &engine.camera );
        RenderPrepare();

        engine.camera.target = AddVector3f( &engine.camera.position, &engine.camera.front );
        world_to_camera = CreateViewMatrix4f( &engine.camera.position, &engine.camera.target, &engine.camera.up );

        LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], 0, &world_to_camera, 0 );
        LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], 0, &world_to_camera, 0 );

        UseProgram( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id );
        // REGION: 
        glUniform3f( glGetUniformLocation( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "viewPos"), 
                     engine.camera.position.components[0], engine.camera.position.components[1], 
                     engine.camera.position.components[2] );

        PlaceLightSourceByName( &engine.camera.position, "spotLight0", &engine.lighting );
        OrientLightSourceByName( &engine.camera.front, "spotLight0", &engine.lighting );
        LoadLightSources( &engine.lighting );

        ActivateAndBindMaterialTextures( &boxMaterial );

        glBindVertexArray( cubeVAO );

        //x+= 0.1f;
        for( int i = 0; i < 10; i++ )
        {
            struct Vector3f object_trans = { (float) i*2 + 5, 0, 0.0f }; //x += 0.1f;
            struct Matrix4f object_model_to_world = CreateTransformationMatrix4f( &object_trans, x, x, 0, 1.0f );
            LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], &object_model_to_world, 0, 0 ); // NOTE: STOPS PROGRAM!!
            
        UseProgram( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id );
            glDrawArrays( GL_TRIANGLES, 0, 36 );
        }

        StopProgram();



        UseProgram( engine.shaderPrograms[SHADER_CATEGORY_LAMP].id );
        glBindVertexArray( lightVAO );

        for( unsigned int i = 0; i < engine.lighting.pointLightCount; i++ )
        {
            struct PointLight pointLight = GetPointLightSourceByIndex( i, &engine.lighting );
            struct Matrix4f object_model_to_world = CreateTransformationMatrix4f( &pointLight.position, 0, 0, 0, 1.0f );
            LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], &object_model_to_world, 0, 0 ); // NOTE: STOPS PROGRAM!!
            
        UseProgram( engine.shaderPrograms[SHADER_CATEGORY_LAMP].id );
            glDrawArrays( GL_TRIANGLES, 0, 36 );
        }

        StopProgram();



        UpdateWindowState( &engine.windowState );
    }


    DestroyEngine( &engine );

}




