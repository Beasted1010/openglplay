// For non-light source objects

// NOTE: "Phong lighting" is used here, where we implement the lighting model in the 
//       fragment shader so that each fragment has the correct light value. This is more
//       computationally expensive than doing all of this in the vertex shader (which is
//       how it used to be done in the old days), but doing it in the vertex shader will
//       lead to interpolation of values which can cause lighting to seem a bit smudgy, 
//       due to there typically being more vertices than fragments. This can be mitigated
//       by having more vertices. Ultimately we get a nice looking bright spot by doing 
//       this per fragment instead of per vertex.



###############|Vertex Shader|################
#version 330 core

// Attributes to Vertex Shader come from attributes in VAO
// Uniforms allow us the flexibility of passing other types of data to our shaders
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

uniform mat4 transformationMatrix;
uniform mat4 modelToWorld; // Model matrix
uniform mat4 worldToCamera; // View matrix
uniform mat4 cameraToClip; // Projection matrix

out vec3 fragmentPos;
out vec3 normal;
out vec2 texCoords;

void main()
{
    // We want the object's position in the world, so use modelToWorld transformation only
    fragmentPos = vec3(modelToWorld * vec4(position, 1.0));

    // Only care about direction. Normalize here for efficiency?
    // The transpose of the inverse of the upper left corner of the model matrix is the
    //      "Normal Matrix" which undoes the effects of nonuniform scaling (skews normals)
    // TODO: This is inefficient to be doing per vertex, best to do on CPU and send over
    //       through a uniform.
    normal = mat3(transpose(inverse(modelToWorld))) * aNormal;

    // Matrices are expected to be column-major order so that I can follow math notation
    gl_Position = cameraToClip * worldToCamera * modelToWorld * vec4(position, 1.0f);
    texCoords = aTexCoords;
}


###############|Fragment Shader|################
#version 330 core

// Per vertex outputs, sent from vertex shader
// Fragment shader interpolates from these values to determine output value of each pixel
in vec3 fragmentPos;
in vec3 normal;
in vec2 texCoords;

// NOTE: Diffuse maps is just a term that means a textured object in a lighted scene
struct Material
{
    //vec3 ambient; -> Removed because ambient almost always = diffuse, no need for both
    //                 and we want a "DIFFUSE MAP" (a lighted texture)
    sampler2D diffuse; // Using sampler to grab from a "diffuse map" to get accurate 
                       // lighting based on the individual materials that make up object
    //vec3 specular; -> Removed because we want a "SPECULAR MAP" (spec intensity texture)
    sampler2D specular;

    float shininess;
};

struct Light
{
    // vec3 position; // No longer necessary when using directional lights
    // vec3 direction; // The direction in which a "directional light" is pointing
    vec4 lightVector; // Used to allow for both position and direction, depending on w comp


    // Using these for flashlight
    // TODO: may want to move these to a flashlight shader?
    // Flashlight is usually at camera's perspective, aimed straight ahead
    // The player position and direction continually update based on orientation & position
    vec3 spotLightPosition;
    vec3 spotLightDirection;
    float innerSpotLightCutoff;
    float outerSpotLightCutoff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};





struct DirectionalLight
{
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
uniform DirectionalLight directionalLight;

vec3 CalculateDirectionalLight( DirectionalLight light, vec3 normal, vec3 viewDirection );


struct PointLight
{
    vec3 position;

    float constant;
    float linear;
    float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
#define POINT_LIGHT_COUNT 4
uniform PointLight pointLights[POINT_LIGHT_COUNT];

vec3 CalculatePointLight( PointLight light, vec3 normal, vec3 fragmentPosition, 
                                            vec3 viewDirection );


uniform vec3 viewPos;
uniform Material material;
uniform Light light;

out vec4 FragColor;

// TODO: Add vec3 CalculateSpotLight() function and add its contributions
void main()
{
    vec3 norm = normalize( normal );
    vec3 viewDirection = normalize( viewPos - fragmentPos );

    vec3 result = CalculateDirectionalLight( directionalLight, norm, viewDirection );

    for( int i = 0; i < POINT_LIGHT_COUNT; i++ )
        result += CalculatePointLight( pointLights[i], norm, fragmentPos, viewDirection );

    FragColor = vec4(result, 1.0f);
}





vec3 CalculateDirectionalLight( DirectionalLight light, vec3 normal, vec3 viewDirection )
{
    vec3 lightDirection = normalize( -light.direction );

    vec3 ambient = light.ambient * texture(material.diffuse, texCoords).rgb;

    vec3 norm = normalize(normal);
    float diff = max(dot(norm, lightDirection), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, texCoords).rgb;

    vec3 reflectDirection = reflect( -lightDirection, norm );
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture(material.specular, texCoords).rgb;

    return (ambient + diffuse + specular);
}

vec3 CalculatePointLight( PointLight light, vec3 normal, vec3 fragmentPosition, 
                                            vec3 viewDirection )
{
    vec3 lightDirection = normalize( light.position - fragmentPosition );

    vec3 ambient = light.ambient * texture(material.diffuse, texCoords).rgb;

    float diff = max(dot(normal, lightDirection), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, texCoords).rgb;

    vec3 reflectDirection = reflect( -lightDirection, normal );
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture(material.specular, texCoords).rgb;

    float distance = length( light.position - fragmentPos );

    float attenuation = 1.0 / ( light.constant + 
                               (light.linear * distance) +
                               (light.quadratic * (distance * distance) ) );

    ambient *= attenuation; 
    diffuse *= attenuation;
    specular *= attenuation;

    return (ambient + diffuse + specular);

}


