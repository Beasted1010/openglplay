
###############|Vertex Shader|################
#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aTexCoord;

out vec2 texCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    // Apply our transformation, model first, then view, then finally projection
    gl_Position = projection * view * model * vec4(aPos, 1.0f);

    texCoord = vec2(aTexCoord.x, aTexCoord.y); // The texture coordinates to pull from
}



###############|Fragment Shader|################
#version 330 core

out vec4 FragColor;

in vec2 texCoord;

// Texture samples
uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
    // Final output color is a combinatin of the two texture lookups
    // "mix" takes two values as input and interpolates between them based on 3rd arg
    // If this 3rd argument is 0.0 it returns the first input val, if 1.0, the second
    // If it is 0.2, it returns 80% of the first and 20% of the second input value
    FragColor = mix(texture(texture1, texCoord), texture(texture2, texCoord), 0.2);
}


