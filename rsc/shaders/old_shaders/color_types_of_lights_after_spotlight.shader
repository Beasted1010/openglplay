// For non-light source objects

// NOTE: "Phong lighting" is used here, where we implement the lighting model in the 
//       fragment shader so that each fragment has the correct light value. This is more
//       computationally expensive than doing all of this in the vertex shader (which is
//       how it used to be done in the old days), but doing it in the vertex shader will
//       lead to interpolation of values which can cause lighting to seem a bit smudgy, 
//       due to there typically being more vertices than fragments. This can be mitigated
//       by having more vertices. Ultimately we get a nice looking bright spot by doing 
//       this per fragment instead of per vertex.



###############|Vertex Shader|################
#version 330 core

// Attributes to Vertex Shader come from attributes in VAO
// Uniforms allow us the flexibility of passing other types of data to our shaders
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

uniform mat4 transformationMatrix;
uniform mat4 modelToWorld; // Model matrix
uniform mat4 worldToCamera; // View matrix
uniform mat4 cameraToClip; // Projection matrix

out vec3 fragmentPos;
out vec3 normal;
out vec2 texCoords;

void main()
{
    // We want the object's position in the world, so use modelToWorld transformation only
    fragmentPos = vec3(modelToWorld * vec4(position, 1.0));

    // Only care about direction. Normalize here for efficiency?
    // The transpose of the inverse of the upper left corner of the model matrix is the
    //      "Normal Matrix" which undoes the effects of nonuniform scaling (skews normals)
    // TODO: This is inefficient to be doing per vertex, best to do on CPU and send over
    //       through a uniform.
    normal = mat3(transpose(inverse(modelToWorld))) * aNormal;

    // Matrices are expected to be column-major order so that I can follow math notation
    gl_Position = cameraToClip * worldToCamera * modelToWorld * vec4(position, 1.0f);
    texCoords = aTexCoords;
}


###############|Fragment Shader|################
#version 330 core

// Per vertex outputs, sent from vertex shader
// Fragment shader interpolates from these values to determine output value of each pixel
in vec3 fragmentPos;
in vec3 normal;
in vec2 texCoords;

// NOTE: Diffuse maps is just a term that means a textured object in a lighted scene
struct Material
{
    //vec3 ambient; -> Removed because ambient almost always = diffuse, no need for both
    //                 and we want a "DIFFUSE MAP" (a lighted texture)
    sampler2D diffuse; // Using sampler to grab from a "diffuse map" to get accurate 
                       // lighting based on the individual materials that make up object
    //vec3 specular; -> Removed because we want a "SPECULAR MAP" (spec intensity texture)
    sampler2D specular;

    float shininess;
};

struct Light
{
    // vec3 position; // No longer necessary when using directional lights
    // vec3 direction; // The direction in which a "directional light" is pointing
    vec4 lightVector; // Used to allow for both position and direction, depending on w comp


    // Using these for flashlight
    // TODO: may want to move these to a flashlight shader?
    // Flashlight is usually at camera's perspective, aimed straight ahead
    // The player position and direction continually update based on orientation & position
    vec3 spotLightPosition;
    vec3 spotLightDirection;
    float innerSpotLightCutoff;
    float outerSpotLightCutoff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

uniform vec3 viewPos;
uniform Material material;
uniform Light light;

out vec4 FragColor;

void main()
{
    // vec3 lightDirection = normalize( light.position - fragmentPos ); // Use directional
    // Allow for both direction and position option in lightVector
    vec3 lightDirection; // TODO currently uninitialized if w != 0 or 1
    // Directional vector has no translation effect, so = 0
    if( light.lightVector.w == 0.0f )
    {
        // Directional
        // Light calculation using a directional light
        lightDirection = normalize( -light.lightVector.xyz );
    }
    // 1 to allow position to account for translation/proj
    else if( light.lightVector.w == 1.0f )
    {
        // Positional
        // Light calculations using light's position
        lightDirection = normalize( light.lightVector.xyz - fragmentPos );
    }



    /*// NOW A SPOTLIGHT EXAMPLE (conditional surrounding light calculations)
    // FIRST DETERMINE IF WE EVEN NEED TO ADD LIGHTING EFFECTS
    // We want dot product to involve the vectors pointing TOWARDS light source
    //     So negate spotLightDir (lightDirection is already orienated correctly)
    float theta = dot( lightDirection, normalize( -light.spotLightDirection ) );
    float epislon = light.innerSpotLightCutoff - light.outerSpotLightCutoff;
    // Clam will "clamp" the first argument between the values as specified (e.g. 0 and 1)
    // This ensures intensity values remain in interval between 0 and 1
    // Possible values of intensity: negative (outside spotlight)
    //                               > 1.0 inside the inner cone of spotlight
    //                               0 < x < 1 somewhere around the edge of spotlight
    float intensity = clamp((theta - light.outerSpotLightCutoff) / epislon, 0.0, 1.0f);*/

    
    // NOTE: We are wanting theta > light.cutOff because cosine of a small angle is a
    //       large value (close to one), and of a large angle (close to 90) is nearly 0
    //       Cosine of the angle is closer to 1 the more the vectors point in same dir
    //       So more they point away from eachother, smaller the cosine value
    //       Cosine of the cutOff angle would be the largest angle (if in spotlight)
    //       So the cosine of the cutoff angle would be the smallest value (if in light)
    // Not needed anymore since the addition of "epsilon" and "intensity" allows for
    //   intensity to be 0 if that fragment is outside of spotlight
    //if( theta > light.innerSpotLightCutoff ) // If so, then do lighting calculations
    {

    //vec3 ambient = material.ambient * light.ambient;
    // Now using diffuse (since ambient is almost always same as diffuse, not storing both)
    //vec3 ambient = material.ambient * light.ambient;
    vec3 ambient = light.ambient * texture(material.diffuse, texCoords).rgb;

    vec3 norm = normalize(normal);
    float diff = max(dot(norm, lightDirection), 0.0);
    //vec3 diffuse = (diff * material.diffuse) * light.diffuse;
    // Now using diffuse texture sampler with sampled location based on texCoords
    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, texCoords).rgb;

    vec3 viewDirection = normalize( viewPos - fragmentPos );
    // -lightDirection because reflect expects vector going FROM light to fragment pos
    vec3 reflectDirection = reflect( -lightDirection, norm );
    // We raise to pow(32) 32=shininess value of the highlight
    //      Higher this value, the more it properly reflects light instead of scattering
    //          it around, so that the bright spot is even smaller
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture(material.specular, texCoords).rgb;

    // NOW A SPOTLIGHT EXAMPLE (conditional surrounding light calculations)
    // FIRST DETERMINE IF WE EVEN NEED TO ADD LIGHTING EFFECTS
    // We want dot product to involve the vectors pointing TOWARDS light source
    //     So negate spotLightDir (lightDirection is already orienated correctly)
    float theta = dot( lightDirection, normalize( -light.spotLightDirection ) );
    float epislon = light.innerSpotLightCutoff - light.outerSpotLightCutoff;
    // Clam will "clamp" the first argument between the values as specified (e.g. 0 and 1)
    // This ensures intensity values remain in interval between 0 and 1
    // Possible values of intensity: negative (outside spotlight)
    //                               > 1.0 inside the inner cone of spotlight
    //                               0 < x < 1 somewhere around the edge of spotlight
    float intensity = clamp((theta - light.outerSpotLightCutoff) / epislon, 0.0, 1.0f);
    diffuse *= intensity;
    specular *= intensity;


    // IF using lightVector as a position -> Handle attenuation effects of distance
    if( light.lightVector.w == 1.0f )
    {
        // Treat as POSITIONAL
        // Distance from light source to fragment
        float distance = length( light.lightVector.xyz - fragmentPos );

        // Attenuation formula = 1 / ( const + (linearTerm * dist) + (quadTerm * dist^2) );
        float attenuation = 1.0 / ( light.constant + 
                                   (light.linear * distance) +
                                   (light.quadratic * (distance * distance) ) );

        // NOTE: We could leave ambient component alone so that ambient lighting is not
        //       decreased over distance, but if we use more than 1 light source all
        //       ambient components will start to stack up, which would be a case where
        //       we WOULD want to attenuate lighting as well.
        //       Play around with and see what works for YOUR ENVIRONMENT.
        //ambient *= attenuation; // Leave out since those outside spotlight (else branch)
                                  // would be brighter at large distances than inside
                                  // We don't want to attenuate ambient (for spotlight)
        ambient *= attenuation; // Added back in due to "epsilon" and "intensity" allowing
                                // value of 0 (which rids diffuse/specular) if outside 
        diffuse *= attenuation;
        specular *= attenuation;
    }

    FragColor = vec4(ambient + diffuse + specular, 1.0f);

    }
    /*else
    {
        //Use ambient light so scene isn't completely dark outside of spotlight
        FragColor = vec4( light.ambient * texture(material.diffuse, texCoords).rgb, 1.0);
    }*/



    //FragColor = vec4(ambient + diffuse + specular, 1.0f);
}


