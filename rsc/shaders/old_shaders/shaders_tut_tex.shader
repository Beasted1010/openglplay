
###############|Vertex Shader|################
#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;

out vec3 ourColor;
out vec2 texCoord;

void main()
{
    gl_Position = vec4(aPos, 1.0);
    ourColor = aColor;
    texCoord = aTexCoord;
}



###############|Fragment Shader|################
#version 330 core

out vec4 FragColor;

in vec3 ourColor;
in vec2 texCoord;

uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
    // Final output color is a combinatin of the two texture lookups
    // "mix" takes two values as input and interpolates between them based on 3rd arg
    // If this 3rd argument is 0.0 it returns the first input val, if 1.0, the second
    // If it is 0.2, it returns 80% of the first and 20% of the second input value
    FragColor = mix(texture(texture1, texCoord), texture(texture2, texCoord), 0.2);
}


