// Shader for widgets (e.g. push buttons, textboxes, checkboxes, sliders)

###############|Vertex Shader|################
#version 330 core

layout (location = 0) in vec4 vertex;

out vec2 passTextureCoord;

uniform mat4 projection;

void main()
{
    gl_Position = projection * vec4( vertex.xy, 0, 1.0 );
    
    passTextureCoord = vertex.zw;
}


###############|Fragment Shader|################
#version 330 core

in vec2 passTextureCoord;

out vec4 FragColor;

uniform sampler2D textureSampler;
uniform vec3 widgetColor;

void main()
{
    FragColor = vec4( texture( textureSampler, passTextureCoord ) ) + vec4( widgetColor, 1.0 );
}

