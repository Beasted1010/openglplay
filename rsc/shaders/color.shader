// For non-light source objects

// NOTE: "Phong lighting" is used here, where we implement the lighting model in the 
//       fragment shader so that each fragment has the correct light value. This is more
//       computationally expensive than doing all of this in the vertex shader (which is
//       how it used to be done in the old days), but doing it in the vertex shader will
//       lead to interpolation of values which can cause lighting to seem a bit smudgy, 
//       due to there typically being more vertices than fragments. This can be mitigated
//       by having more vertices. Ultimately we get a nice looking bright spot by doing 
//       this per fragment instead of per vertex.
//
//       light.direction -> People like to specify as from light TO fragment
//                          Our claculations require fragment TO fragment (due to normal)
//                          So we negate this in our computations.
//                          This is speicifed for the DirectionalLight and SpotLight
//                          (PointLight shows same light in all directions)
//
//       reflect() funciton expects first vector (reflectionDirection) to point FROM
//                 the light source TO the fragment position, so we actually use the
//                 original direction of light as given (from source to fragment)
//
//       Specular calculations -> The more the viewer is in reflectDirection, the more
//                 (brighter the) "glare" they will get
//
//



###############|Vertex Shader|################
#version 330 core

// Attributes to Vertex Shader come from attributes in VAO
// Uniforms allow us the flexibility of passing other types of data to our shaders
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

uniform mat4 transformationMatrix;
uniform mat4 modelToWorld; // Model matrix
uniform mat4 worldToCamera; // View matrix
uniform mat4 cameraToClip; // Projection matrix

out vec3 fragmentPos;
out vec3 normal;
out vec2 texCoords;

void main()
{
    // We want the object's position in the world, so use modelToWorld transformation only
    fragmentPos = vec3(modelToWorld * vec4(position, 1.0));

    // Only care about direction. Normalize here for efficiency?
    // The transpose of the inverse of the upper left corner of the model matrix is the
    //      "Normal Matrix" which undoes the effects of nonuniform scaling (skews normals)
    // TODO: This is inefficient to be doing per vertex, best to do on CPU and send over
    //       through a uniform.
    normal = mat3(transpose(inverse(modelToWorld))) * aNormal;

    // Matrices are expected to be column-major order so that I can follow math notation
    gl_Position = cameraToClip * worldToCamera * modelToWorld * vec4(position, 1.0f);
    texCoords = aTexCoords;
}


###############|Fragment Shader|################
#version 330 core

// Per vertex outputs, sent from vertex shader
// Fragment shader interpolates from these values to determine output value of each pixel
in vec3 fragmentPos;
in vec3 normal;
in vec2 texCoords;

// NOTE: Diffuse maps is just a term that means a textured object in a lighted scene
struct Material
{
    //vec3 ambient; -> Removed because ambient almost always = diffuse, no need for both
    //                 and we want a "DIFFUSE MAP" (a lighted texture)
    sampler2D diffuse; // Using sampler to grab from a "diffuse map" to get accurate 
                       // lighting based on the individual materials that make up object
    //vec3 specular; -> Removed because we want a "SPECULAR MAP" (spec intensity texture)
    sampler2D specular;

    float shininess;
};
uniform Material material;


struct DirectionalLight
{
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
#define MAX_DIRECTIONAL_LIGHT_COUNT 10
uniform int directionalLightCount;
uniform DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHT_COUNT];

vec3 CalculateDirectionalLight( DirectionalLight light, vec3 normal, vec3 viewDirection );

struct PointLight
{
    vec3 position;

    float constant;
    float linear;
    float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
// TODO: Make this dynamic -> 1 way: Add this line to fragment shader before compiling...
//                                   But this requires we check which shader file we use
#define MAX_POINT_LIGHT_COUNT 10
uniform int pointLightCount;
uniform PointLight pointLights[MAX_POINT_LIGHT_COUNT];

vec3 CalculatePointLight( PointLight light, vec3 normal, vec3 fragmentPosition, 
                                            vec3 viewDirection );

struct SpotLight
{
    vec3 position;
    vec3 direction;
    float innerCutoff;
    float outerCutoff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};
#define MAX_SPOT_LIGHT_COUNT 10
uniform int spotLightCount;
uniform SpotLight spotLights[MAX_SPOT_LIGHT_COUNT];

vec3 CalculateSpotLight( SpotLight light, vec3 normal, vec3 fragmentPosition,
                                          vec3 viewDirection  );



uniform vec3 viewPos;

out vec4 FragColor;

void main()
{
    vec3 norm = normalize( normal );
    vec3 viewDirection = normalize( viewPos - fragmentPos );

    vec3 result = {0, 0, 0};

    // TODO: This seems to cause problems when a uint (doesn't compile)
    //       Because of this, I am using int for pointLightCount type
    for( int i = 0; i < directionalLightCount; i++ )
        result += CalculateDirectionalLight( directionalLights[i], 
                                                 norm, viewDirection );

    for( int i = 0; i < pointLightCount; i++ )
        result += CalculatePointLight( pointLights[i], norm, fragmentPos, viewDirection );

    for( int i = 0; i < spotLightCount; i++ )
        result += CalculateSpotLight( spotLights[i], norm, fragmentPos, viewDirection );

    FragColor = vec4(result, 1.0f);
}





vec3 CalculateDirectionalLight( DirectionalLight light, vec3 normal, vec3 viewDirection )
{
    vec3 lightDirection = normalize( -light.direction );

    vec3 ambient = light.ambient * texture(material.diffuse, texCoords).rgb;

    float diff = max(dot(normal, lightDirection), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, texCoords).rgb;

    vec3 reflectDirection = reflect( -lightDirection, normal );
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture(material.specular, texCoords).rgb;

    return (ambient + diffuse + specular);
}

vec3 CalculatePointLight( PointLight light, vec3 normal, vec3 fragmentPosition, 
                                            vec3 viewDirection )
{
    vec3 lightDirection = normalize( light.position - fragmentPosition );

    vec3 ambient = light.ambient * texture(material.diffuse, texCoords).rgb;

    float diff = max(dot(normal, lightDirection), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, texCoords).rgb;

    vec3 reflectDirection = reflect( -lightDirection, normal );
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture(material.specular, texCoords).rgb;

    float distance = length( light.position - fragmentPos );

    float attenuation = 1.0 / ( light.constant + 
                               (light.linear * distance) +
                               (light.quadratic * (distance * distance) ) );

    ambient *= attenuation; 
    diffuse *= attenuation;
    specular *= attenuation;

    return (ambient + diffuse + specular);
}


vec3 CalculateSpotLight( SpotLight light, vec3 normal, vec3 fragmentPosition,
                                          vec3 viewDirection  )
{
    vec3 lightDirection = normalize( light.position - fragmentPosition );

    // ambient
    vec3 ambient = light.ambient * texture( material.diffuse, texCoords ).rgb;

    // diffuse -> Need to consider light's direction
    float diff = max(dot(normal, lightDirection), 0.0); // diffuse contirbution
    vec3 diffuse = light.diffuse * diff * texture( material.diffuse, texCoords ).rgb;

    // specular -> Need to consider camera's (views) position
    vec3 reflectDirection = reflect( -lightDirection, normal ); // Reflect over normalal
    float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture( material.specular, texCoords ).rgb;

    // spotlight (to allow for soft edges, instead of an obvious circle of light)
    float theta = dot( lightDirection, normalize( -light.direction ) );
    float epsilon = (light.innerCutoff - light.outerCutoff);
    float intensity = clamp((theta - light.outerCutoff) / epsilon, 0.0, 1.0f);
    ambient *= intensity;
    diffuse *= intensity;
    specular *= intensity;

    // attenuation -> Further away should cause object to seem less lit
    float distance = length( light.position - fragmentPosition );
    float attenuation = 1.0 / ( light.constant + (light.linear * distance) + 
                               (light.quadratic * (distance * distance)) );
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    return (ambient + diffuse + specular);
}

