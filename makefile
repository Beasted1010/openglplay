
INC=inc
SRC=src
OBJ=obj

MK_LIB = ar rcs
LIB_NAME = FYEngine.a

main = main.c

OUT=run_tut

CC=g++
CFLAGS=-Wall -DGLEW_STATIC -Iinc -Idependencies/GLEW/include -I"C:\glfw-3.2.1.bin.WIN64\glfw-3.2.1.bin.WIN64\include" \
						   -I"C:\Users\die20\Documents\Code\Libraries\FYEngine\inc"
LINKFLAGS= -L C:\glfw-3.2.1.bin.WIN64\glfw-3.2.1.bin.WIN64\lib-mingw-w64 \
   		   -L C:\Users\die20\Documents\Code\OpenGLPlay\dependencies\GLEW\lib\Release\x64 \
		   -L C:\Users\die20\Documents\Code\Libraries\FYEngine\lib
LIBRARIES= -lfyengine -lglew32s -lopengl32 -lglfw3 -lgdi32


_DEPS = lighting.h
DEPS = $(patsubst %, $(INC)/%, $(_DEPS))
SOURCES = $(SRC)/$(main) $(subst .h,.c, $(patsubst %, $(SRC)/%, $(_DEPS)))
OBJECTS = $(OBJ)/$(subst .c,.o,$(main)) $(subst .h,.o, $(patsubst %, $(OBJ)/%, $(_DEPS)))

$(OBJ)/%.o: $(SRC)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(OUT): $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(LINKFLAGS) $(LIBRARIES)


library: $(SOURCES)
	$(MK_LIB) $(LIB_NAME) $(OBJECTS)


help:
	echo sources = $(SOURCES)
	echo objects = $(OBJECTS)
	echo command = $(CC) $(CFLAGS) $(LINKFLAGS) $(SOURCES) $(LIBRARIES)

clean:
	del /q $(OBJ)\*


